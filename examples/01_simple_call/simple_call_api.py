from ringding import RdServer


class Api:
    def hello(self):
        return 'Hello world!'

    def sum(self, a, b):
        return a + b


if __name__ == '__main__':
    RdServer().serve(Api)
