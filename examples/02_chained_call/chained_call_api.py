import dataclasses
from typing import Dict

from ringding import RdServer


@dataclasses.dataclass
class Person:
    name: str
    age: int
    sex: str

    def summarize_age(self, years_to_add: int):
        return self.age + years_to_add


class Api:
    def __init__(self):
        self._persons: Dict[str, Person] = {}

    def hello(self):
        return 'Hello world!'

    def add_person(self, name: str, age: int, sex: str):
        self._persons[name] = Person(name, age, sex)

    def get_person(self, name: str) -> Person:
        return self._persons[name]


if __name__ == '__main__':
    RdServer().serve(Api)
