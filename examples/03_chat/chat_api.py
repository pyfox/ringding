import datetime
from typing import List

import ringding
from _ringding.client.base import BaseClient

CHAT_LOG: List[str] = []


@ringding.notification('Notifications.new_message')
def new_message_received(message: str):
    return message


@ringding.notification('Notifications.new_user')
def new_user(username: str):
    return username


@ringding.notification('Notifications.user_left')
def user_left(username: str):
    return username


class ChatApi:
    @ringding.use_client
    def message(self, message: str, _client: ringding.RdClient):
        """
        The clients call this to send a new message.

        :param message: The message text
        :param _client: The client that calls this method. This is injected by the
                        @ringding.use_client decorator.
        """
        sender = _client.get_handshake_data()['username']
        timestamp = datetime.datetime.now().strftime('%H:%M:%S')

        full_message = f'{timestamp} {sender}: {message}'

        CHAT_LOG.append(full_message)
        _client.get_server().broadcast(new_message_received(full_message))

    def fetch_log(self) -> List[str]:
        """
        The clients can call this to get a log.

        :return: A list of messages
        """
        return CHAT_LOG

    @ringding.use_client
    def fetch_connected_users(self, _client: ringding.RdClient) -> List[str]:
        """
        The client can call this to fetch a list of users that are currently connected.

        :param _client: The client. It will be injected by the @ringding.use_client
                        decorator.
        :return: A list of connected usernames.
        """
        connected_users = []
        for user in _client.get_server().get_clients().values():
            connected_users.append(user.get_handshake_data()['username'])
        return connected_users


class ChatServer(ringding.RdServer):
    def on_client_connect(self, client: ringding.BaseClient):
        """
        This is called when a new client connects.

        :param client: The client that connected
        """
        client_name = client.get_handshake_data()['username']
        self.broadcast(new_user(client_name))

    def on_client_disconnect(self, client: BaseClient):
        """
        This is called when a client disconnects.

        :param client: The client that disconnected.
        """
        client_name = client.get_handshake_data()['username']
        self.broadcast(user_left(client_name))


if __name__ == '__main__':
    ChatServer(port=42000).serve(ChatApi)
