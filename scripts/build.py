import json
import os
import pathlib
import re
import shutil
import subprocess
import sys

_BASE_PATH = pathlib.Path(__file__).parent.parent
_SRC_PATH = _BASE_PATH / 'src'
_BUILD_DIR = _BASE_PATH / 'deploy'
shutil.rmtree(_BUILD_DIR, ignore_errors=True)
_BUILD_DIR.mkdir()


def run_process(cmd, env=None, cwd=None):
    env = env or {}
    env = {**env, **os.environ}
    print(f'Execute "{cmd}"')
    process = subprocess.Popen(cmd, shell=True, stderr=sys.stderr, stdout=sys.stdout,
                               cwd=cwd or _BASE_PATH, env=env)
    process.communicate()
    if process.returncode != 0:
        raise RuntimeError(f'Process "{cmd}" did not finish properly.')


def build():
    print('*** BUILD RINGDING ***')
    run_process(f'python setup.py bdist_wheel --dist-dir {_BUILD_DIR.as_posix()}/ringding',
                {'RINGDING_RELEASE_VERSION': get_release_name()})
    build_npm_release()
    shutil.rmtree(_BASE_PATH / 'build')


def build_npm_release():
    print('*** BUILD RINGDINGJS ***')
    print('Copying files...')
    tmp_dir = _BUILD_DIR / 'ringdingjs'
    tmp_dir.mkdir()
    shutil.copy(_SRC_PATH / '_ringding' / 'resources' / 'js' / 'package.json', tmp_dir)
    shutil.copy(_SRC_PATH / '_ringding' / 'resources' / 'js' / 'ringdingjs.js', tmp_dir)
    shutil.copy(_BASE_PATH / 'LICENSE', tmp_dir)
    shutil.copy(_BASE_PATH / 'README.md', tmp_dir)

    print(f'Replace version in {tmp_dir.as_posix()}/package.json')
    with (tmp_dir / 'package.json').open('r', encoding='utf-8') as file:
        package_json = json.load(file)
    package_json['version'] = get_release_name()
    with (tmp_dir / 'package.json').open('w', encoding='utf-8') as file:
        json.dump(package_json, file, indent=2)
    run_process('npm install --dry-run', cwd=tmp_dir)


def get_release_name() -> str:
    current_branch = os.environ.get('CURRENT_GIT_BRANCH_NAME', 'NO_BRANCH_SPECIFIED')
    current_branch = current_branch.strip()
    print(f'Current branch: {current_branch}')
    release_branch_match = 'release/(\d+.\d+.\d)'
    if not re.match(release_branch_match, current_branch):
        return 'develop'

    return re.findall(release_branch_match, current_branch)[0]


build()
