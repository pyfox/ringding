import pathlib

PACKAGE_JSON = pathlib.Path(__file__).parent / 'package.json'
RINGDINGJS = pathlib.Path(__file__).parent / 'ringdingjs.js'
