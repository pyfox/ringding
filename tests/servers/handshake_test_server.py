import contextlib
import logging
import pathlib
import subprocess
import sys
import threading
import time
from typing import Any, Dict

import ringding


class SecretApi:
    def __init__(self):
        pass

    @staticmethod
    def get_secret():
        return 21

    @ringding.use_client
    def get_handshake_data(self, _client):
        return _client.get_handshake_data()


class AdminApi(SecretApi):
    @staticmethod
    def get_admin_secret():
        return 42


class HandshakeTestServer(ringding.RdServer):
    def client_factory(self, entrypoint: object,
                       handshake_data: Dict[str, Any]) -> ringding.RdClientBase:
        if handshake_data['secret'] == 'donttellanybody':
            return ringding.RdClient(SecretApi, handshake_data)
        elif handshake_data['secret'] == 'admin_secret':
            return ringding.RdClient(AdminApi, handshake_data)
        # We don't return anything here intentionally to check that invalid return types are
        # handled in an acceptable manner.


@contextlib.contextmanager
def run_handshake_test_server():
    server = HandshakeTestServer('127.0.0.1', 37008, log_level=logging.DEBUG)

    t = threading.Thread(target=lambda: server.serve(SecretApi))
    t.start()
    server.wait_until_started()
    yield
    server.stop()


if __name__ == '__main__':
    with run_handshake_test_server():
        logging.info('Start test server, press Ctrl+C to exit')
        while True:
            time.sleep(1)
