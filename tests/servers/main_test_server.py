import contextlib
import dataclasses
import logging
import pathlib
import subprocess
import sys
import threading
import time

import ringding
from _ringding.notifications import notification
from ringding import use_client


@notification('something_happened_event')
def something_happened_event(timestamp: int, happening: str):
    return [timestamp, happening]


@notification('calculation_finished_event')
@dataclasses.dataclass
class CalculationFinishedEvent:
    """Some documentation."""
    timestamp: float
    result: int


class EventApi:
    @notification('EventApi.event_api_event')
    def EventApiEvent(self, random_number: int):
        return {'random_number': random_number}

    @use_client
    def send_event(self, _client):
        random_value = 84
        _client.notify(self.EventApiEvent(random_value))
        _client.notify(CalculationFinishedEvent(timestamp=123.456, result=42))
        _client.notify(something_happened_event(timestamp=456, happening='boo'))
        return random_value


class DataTypeApi:
    def get_list(self):
        return [1, 2, 3, 'a', 'b', 'c']

    def get_dict(self):
        return {'a': 42, 'b': 32, 'c': {'ca': 'xx', 'cb': 'yy'}}

    def get_object(self):
        class XYZ:
            CLASS_ATTRIBUTE = 'abc'

            def __init__(self):
                self._INSTANCE_ATTRIBUTE = 'def'

            def __str__(self):
                return 'I am some object'

            def some_method(self):
                return None

        return XYZ()

    def get_dataclass(self):
        @dataclasses.dataclass
        class Data:
            number: int
            string: str

        return Data(42, 'heyho')


class PetApi:
    FAVORITE_PET = 'fox'

    def __init__(self):
        self._hidden_pet = 'cat'
        self._pets = []

    def add_animal(self, name: str):
        self._pets.append(name)

    def get_animal_count(self, name: str = ''):
        if name:
            return self._pets.count(name)
        else:
            return len(self._pets)


class User:
    def __init__(self):
        self._cookies = []

    def give_cookie(self, cookie: str):
        self._cookies.append(cookie)
        return f'Thank you for a {cookie} cookie :)'


class TestApi:
    EventApi = EventApi()

    def __init__(self):
        self._users = {
            0: User(),
            1: User()
        }

    @property
    def PetApi(self):
        return PetApi()

    @property
    def DataTypeApi(self):
        return DataTypeApi()

    @ringding.use_client
    def retrieve_value_from_client(self, _client):
        return _client.abc

    @ringding.use_client
    def store_value_on_client(self, value: int, _client):
        _client.abc = value

    def hello(self):
        return 'hello'

    def sum(self, a, b):
        return a + b

    def get_user(self, user_id: int):
        try:
            return self._users[user_id]
        except KeyError:
            raise ValueError(f'Did not find a user with ID {user_id}')

    def _protected_function(self):
        return 'I am protected and should not be called.'

    def __private_function(self):
        return 'I am private and should not be called.'


@contextlib.contextmanager
def run_test_server():
    server = ringding.RdServer('127.0.0.1', 37007, log_level=logging.DEBUG)
    t = threading.Thread(target=lambda: server.serve(TestApi))
    t.start()
    server.wait_until_started()
    yield t
    server.stop()


if __name__ == '__main__':
    with run_test_server() as t:
        logging.info('Start test server, press Ctrl+C to exit')
        t.join()
        # while True:
        #     time.sleep(1)
