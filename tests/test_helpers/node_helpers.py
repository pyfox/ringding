import dataclasses
import logging
import os
import pathlib
import subprocess
from typing import Dict, Optional


@dataclasses.dataclass
class CallResults:
    stdout: str
    stderr: str
    returncode: int

    def get_log(self):
        return '\n'.join([self.stdout, self.stderr])


def execute_command(cmd: str,
                    cwd: pathlib.Path = pathlib.Path(__file__).parent.parent / 'clients',
                    env: Optional[Dict] = None):
    """Execute a command on command line."""
    if env:
        env = {**os.environ, **env}

    logging.info(f"Running command '{cmd}'")
    result = subprocess.run(cmd, cwd=str(cwd), stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE, shell=True, env=env)
    return CallResults((result.stdout or b'').decode('utf-8'),
                       (result.stderr or b'').decode('utf-8'),
                       result.returncode)
