import os
import pathlib

import pytest
from _ringding.resources.js import RINGDINGJS

from test_helpers.node_helpers import execute_command

COMMON_FILE_PATH = pathlib.Path(__file__).parent / 'js' / 'common'
HANDSHAKE_FILE_PATH = pathlib.Path(__file__).parent / 'js' / 'handshake'

JS_TESTS = [path.name for path in COMMON_FILE_PATH.glob('test*.js')]
HANDSHAKE_TESTS = [path.name for path in HANDSHAKE_FILE_PATH.glob('test*.js')]


def _retrieve_client_path() -> str:
    """
    Retrieve the client path and run npm install if node_modules folder does not
    exists.
    """
    client_path = pathlib.Path(os.environ.get('RINGDINGJS_TEST_CLIENT_PATH',
                                              RINGDINGJS)).absolute()
    client_folder = client_path.parent
    print('Client path', client_path)

    if not client_path.is_file():
        raise FileNotFoundError(
            f'The provided path {client_path} does not exist.'
            f' Please provide the path to ringdingjs.js in '
            f'RINGDINGJS_TEST_CLIENT_PATH!')

    if not (client_folder / 'node_modules').exists():
        print('Running npm install...')
        execute_command('npm install', cwd=client_folder)
    return client_path.as_posix().replace('.js', '')


@pytest.mark.parametrize('file', JS_TESTS)
def test_node_client(file: pathlib.Path):
    client_path = _retrieve_client_path()

    print(f'Using ringdingjs from {client_path}')
    result = execute_command(f'node {file}', COMMON_FILE_PATH,
                             env={'RINGDINGJS_TEST_CLIENT_PATH': client_path})
    assert result.returncode == 0, result.get_log()


@pytest.mark.parametrize('file', HANDSHAKE_TESTS)
def test_node_client_handshake(file: pathlib.Path):
    client_path = _retrieve_client_path()

    print(f'Using ringdingjs from {client_path}')
    result = execute_command(f'node {file}', HANDSHAKE_FILE_PATH,
                             env={'RINGDINGJS_TEST_CLIENT_PATH': client_path})
    assert result.returncode == 0, result.get_log()
