import pytest

from servers.handshake_test_server import run_handshake_test_server
from servers.main_test_server import run_test_server


@pytest.fixture(scope='session', autouse=True)
def run_server():
    """This fixture will start all servers which are required for the test.."""
    with run_test_server():
        with run_handshake_test_server():
            yield
