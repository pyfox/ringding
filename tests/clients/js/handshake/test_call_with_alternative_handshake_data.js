const {RD} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    await RD.connect('ws://localhost:37008', {'secret': 'admin_secret'}, logger)
    let secret = await RD.call('AdminApi.get_secret()')
    let admin_secret = await RD.call('AdminApi.get_admin_secret()')
    console.log('Network traffic', LOG)
    assert.equal(secret, 21)
    assert.equal(admin_secret, 42)
    assert(LOG.includes('>>> {"cmd":"handshake","type":1,"id":0,"param":{"secret":"admin_secret"}}'))
    assert(LOG.includes('<<< {"id": 0, "data": "", "type": 2}'))
    assert(LOG.includes('>>> {"cmd":"AdminApi.get_secret()","type":1,"id":1}'))
    assert(LOG.includes('<<< {"id": 1, "data": 21, "type": 2}'))
    assert(LOG.includes('>>> {"cmd":"AdminApi.get_admin_secret()","type":1,"id":2}'))
    assert(LOG.includes('<<< {"id": 2, "data": 42, "type": 2}'))
    await RD.disconnect()
    LOG = []
}

test()
