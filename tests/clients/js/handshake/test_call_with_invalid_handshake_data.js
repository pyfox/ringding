const {RD} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    try {
        await RD.connect('ws://localhost:37008', {'secret': 'wrongsecret'}, logger)
        console.log('Network traffic', LOG)
        assert(0)  // Should never be reached.
    } catch(e) {
        console.log('Network traffic', LOG)
        assert(LOG.includes('>>> {"cmd":"handshake","type":1,"id":0,"param":{"secret":"wrongsecret"}}'))
        assert(LOG.includes('<<< {"id": 0, "error": "Handshake did not succeed: Client factory did return invalid client \\"None\\"", "type": 3}'))
    }
    await RD.disconnect()
    LOG = []
}

test()
