const {RD} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    const handshake_data = {'secret': 'donttellanybody'}
    await RD.connect('ws://localhost:37008', handshake_data, logger)
    let secret = await RD.call('SecretApi.get_handshake_data()')
    console.log('Network traffic', LOG)
    assert.equal(Object.keys(secret).length, 1)
    assert.equal(secret.secret, handshake_data.secret)
    await RD.disconnect()
    LOG = []
}

test()
