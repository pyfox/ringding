const {RD} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    await RD.connect('ws://localhost:37008', {'secret': 'donttellanybody'}, logger)
    let secret = await RD.call('SecretApi.get_secret()')
    console.log('Network traffic', LOG)
    assert.equal(secret, 21)
    assert(LOG.includes('>>> {"cmd":"handshake","type":1,"id":0,"param":{"secret":"donttellanybody"}}'))
    assert(LOG.includes('<<< {"id": 0, "data": "", "type": 2}'))
    assert(LOG.includes('>>> {"cmd":"SecretApi.get_secret()","type":1,"id":1}'))
    assert(LOG.includes('<<< {"id": 1, "data": 21, "type": 2}'))
    await RD.disconnect()
    LOG = []
}

test()
