const {RD} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    await RD.connect('ws://localhost:37007', null, logger)
    let response = await RD.call('TestApi.sum(a, b)', {a: 5, b: 3})
    assert.equal(8, response)

    console.log('Network traffic', LOG)
    assert(LOG.includes('>>> {"cmd":"TestApi.sum(a, b)","type":1,"id":1,"param":{"a":5,"b":3}}'))
    assert(LOG.includes('<<< {"id": 1, "data": 8, "type": 2}'))
    RD.disconnect()
}

test()
