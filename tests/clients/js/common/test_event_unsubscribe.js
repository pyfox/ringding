const {RD, CallApi} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    await RD.connect('ws://localhost:37007', null, logger)
    let event_counter = 0
    function count_event() {
        event_counter += 1
    }
    RD.register_event('EventApi.event_api_event', count_event)
    await CallApi().TestApi.EventApi.send_event()
    console.log(LOG)
    assert(event_counter === 1)
    await CallApi().TestApi.EventApi.send_event()
    assert(event_counter === 2)

    // Unsubscribe
    RD.unregister_event('EventApi.event_api_event', count_event)
    await CallApi().TestApi.EventApi.send_event()
    assert(event_counter === 2)

    // Resubscribe
    RD.register_event('EventApi.event_api_event', count_event)
    await CallApi().TestApi.EventApi.send_event()
    assert(event_counter === 3)
    await RD.disconnect()
    LOG = []
}

test()
