const {RD} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    await RD.connect('ws://localhost:37007', null, logger)
    try {
        await RD.call('TestApi.thisdoesnotexist()')
        caught_exception = false
    } catch (e) {
        caught_exception = true
        assert(String(e).includes('Error while executing thisdoesnotexist({}) of TestApi.thisdoesnotexist(): \'TestApi\' object has no attribute \'thisdoesnotexist\''))
    } finally {
        console.log('Network traffic', LOG)
    }
    assert(caught_exception)
    RD.disconnect()
}

test()
