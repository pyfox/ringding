const {RD, CallApi} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    await RD.connect('ws://localhost:37007', null, logger)
    let event_counter = 0
    let event_counter_2 = 0

    function count_event() {
        event_counter += 1
    }
    function count_event_2() {
        event_counter_2 += 1
    }

    RD.register_event('EventApi.event_api_event', count_event)
    RD.register_event('EventApi.event_api_event', count_event_2)

    await CallApi().TestApi.EventApi.send_event()
    console.log(LOG)
    LOG = []
    assert(event_counter === 1)
    assert(event_counter_2 === 1)

    await CallApi().TestApi.EventApi.send_event()
    console.log(LOG)
    LOG = []
    assert(event_counter === 2)
    assert(event_counter_2 === 2)

    RD.unregister_event('EventApi.event_api_event', count_event)

    CallApi().TestApi.EventApi.send_event()
    console.log(LOG)
    LOG = []
    await RD.wait_for_event('EventApi.event_api_event')
    assert(event_counter === 2)
    assert(event_counter_2 === 3)

    await CallApi().TestApi.EventApi.send_event()
    console.log(LOG)
    LOG = []
    assert(event_counter === 2)
    assert(event_counter_2 === 4)

    await RD.disconnect()
}

test()
