const {RD, CallApi} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    await RD.connect('ws://localhost:37007', null, logger)
    let event_counter = 0
    function count_event(data) {
        if (data['random_number'] === 84) {
            event_counter += 1
        }
    }

    // Test await of an event when no listener is listening.
    CallApi().TestApi.EventApi.send_event()
    const data = await RD.wait_for_event('EventApi.event_api_event')
    console.log(LOG)
    LOG = []
    assert(data['random_number'] === 84)

    // Test await if an event listener is listening on that same event.
    RD.register_event('EventApi.event_api_event', count_event)
    CallApi().TestApi.EventApi.send_event()
    const data2 = await RD.wait_for_event('EventApi.event_api_event')
    console.log(LOG)
    LOG = []
    assert(data2['random_number'] === 84)
    assert(event_counter === 1)

    // Test await if a event_listener was removed from that event.
    RD.unregister_event('EventApi.event_api_event', count_event)
    CallApi().TestApi.EventApi.send_event()
    const data3 = await RD.wait_for_event('EventApi.event_api_event')
    console.log(LOG)
    LOG = []
    assert(data3['random_number'] === 84)
    assert(event_counter === 1)

    await RD.disconnect()
}

test()
