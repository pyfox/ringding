const {RD} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    for (let i=0; i < 10; i++) {
        await RD.connect('ws://localhost:37007', null, logger)
        let response = await RD.call('TestApi.hello()')
        assert.equal('hello', response)
        assert(LOG.includes('>>> {"cmd":"TestApi.hello()","type":1,"id":1}'))
        assert(LOG.includes('<<< {"id": 1, "data": "hello", "type": 2}'))
        await RD.disconnect()
    }
}
test()
