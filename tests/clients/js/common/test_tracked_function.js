const {RD, CallApi} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    await RD.connect('ws://localhost:37007', null, logger)

    const value_to_set = Math.random()

    await CallApi().TestApi.store_value_on_client({value: value_to_set})
    const result = await CallApi().TestApi.retrieve_value_from_client()
    console.log(LOG)
    assert.equal(result, value_to_set)

    await RD.disconnect()
    LOG = []
}

test()
