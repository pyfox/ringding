const http = require('http')
const assert = require('assert')

let check_succeeded = false;


/**
 * Check that the client code is reachable on /client.js endpoint.
 * This way it can be embedded into html documents, or fetched by the node client e.g.
 * with libraries like require-from-url.
 */
function test() {
    http.get('http://localhost:37007/client.js', res => {
        if (res.statusCode === 200 && /^text\/javascript/.test(res.headers['content-type'])) {
            let rawData = '';
            res.setEncoding('utf8');
            res.on('data', chunk => {
                rawData += chunk;
            });
            res.on('end', () => {
                console.log(rawData)

                // Check if first line of ringdingjs exists.
                // Background: Between header and body must be an empty line. Otherwise
                // everything until the first empty line is interpreted as header.

                assert(rawData.startsWith('let _w3cWebSocket;'))
                assert(rawData.includes('class _ringdingjs'))
                check_succeeded = true
                clearTimeout(TIMEOUT)
            });
        }
    });

    const TIMEOUT = setTimeout(function () {
            assert(check_succeeded === true)
        },
        500)
}

test()
