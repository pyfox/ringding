const {RD} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test_explicit_call() {
    await RD.connect('ws://localhost:37007', null, logger)
    RD.notify('TestApi.hello()')

    console.log('Network traffic', LOG)
    assert(LOG.includes('>>> {"cmd":"TestApi.hello()","type":0}'))
    await RD.disconnect()
    LOG = []
}

test_explicit_call()
