const {RD} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function call_invalid_function(name) {
    let caught_exception
    try {
        await RD.call(name)
        caught_exception = false
    } catch (e) {
        caught_exception = true
        console.log('Error: ', String(e))
        assert(String(e).includes(`You are not allowed to access the private member ${name}.`))
    } finally {
        console.log('Network traffic', LOG)
    }
    assert(caught_exception)
}

async function test() {
    await RD.connect('ws://localhost:37007', null, logger)
    console.log('A')
    await call_invalid_function('TestApi._protected_function()')
    console.log('B')
    await call_invalid_function('TestApi.__private_function()')
    console.log('C')
    await call_invalid_function('TestApi.PetApi._hidden_pet')
    RD.disconnect()
}

test()
