const {RD, CallApi} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    await RD.connect('ws://localhost:37007', null, logger)
    let event_a_called = false
    let event_b_called = false
    let event_c_called = false
    RD.register_event('EventApi.event_api_event', function (data) {
        if (data['random_number'] === 84) {
            event_a_called = true
        }
    })
    RD.register_event('calculation_finished_event', function (data) {
        if (data['timestamp'] === 123.456 && data['result'] === 42) {
            event_b_called = true
        }
    })
    RD.register_event('something_happened_event', function (data) {
        if (data[0] === 456 && data[1] === 'boo') {
            event_c_called = true
        }
    })
    await CallApi().TestApi.EventApi.send_event()
    console.log(LOG)
    assert(LOG.includes('>>> {"cmd":"handshake","type":1,"id":0}'))
    assert(LOG.includes('<<< {"id": 0, "data": "", "type": 2}'))
    assert(LOG.includes('>>> {"cmd":"TestApi.EventApi.send_event()","type":1,"id":1}'))
    assert(LOG.includes('<<< {"cmd": "EventApi.event_api_event", "data": {"random_number": 84}, "type": 0}'))
    assert(LOG.includes('<<< {"cmd": "calculation_finished_event", "data": {"timestamp": 123.456, "result": 42}, "type": 0}'))
    assert(LOG.includes('<<< {"cmd": "something_happened_event", "data": [456, "boo"], "type": 0}'))
    assert(LOG.includes('<<< {"id": 1, "data": 84, "type": 2}'))
    assert(event_a_called === true)
    assert(event_b_called === true)
    assert(event_c_called === true)

    await RD.disconnect()
    LOG = []
}

test()
