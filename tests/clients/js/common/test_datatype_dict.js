const {RD, CallApi} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    await RD.connect('ws://localhost:37007', null, logger)
    let response = await CallApi().TestApi.DataTypeApi.get_dict()
    console.log('Network traffic', LOG)
    const expected_dict = {a: 42, b: 32, c: {ca: 'xx', cb: 'yy'}}
    assert(JSON.stringify(response) === JSON.stringify(expected_dict))
    assert(response.a === 42)
    assert(response.c.ca === 'xx')
    RD.disconnect()
    LOG = []
}

test()
