const {RD, CallApi} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    await RD.connect('ws://localhost:37007', null, logger)
    let response = await CallApi().TestApi.DataTypeApi.get_list()
    assert.equal(response[0], 1)
    assert.equal(response[1], 2)
    assert.equal(response[2], 3)
    assert.equal(response[3], 'a')
    assert.equal(response[4], 'b')
    assert.equal(response[5], 'c')
    assert.equal(response.length, 6)
    console.log('Network traffic', LOG)
    RD.disconnect()
    LOG = []
}

test()
