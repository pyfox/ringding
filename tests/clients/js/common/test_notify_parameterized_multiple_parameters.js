const {RD} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    await RD.connect('ws://localhost:37007', null, logger)
    RD.notify('TestApi.sum(a, b)', {a: 5, b: 3})

    console.log('Network traffic', LOG)
    assert(LOG.includes('>>> {"cmd":"TestApi.sum(a, b)","type":0,"param":{"a":5,"b":3}}'))
    RD.disconnect()
}

test()
