const {RD, CallApi} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    await RD.connect('ws://localhost:37007', null, logger)
    let response = await CallApi().TestApi.DataTypeApi.get_dataclass()
    console.log('Network traffic', LOG)
    const expected_response = {number: 42, string: 'heyho'}
    assert(JSON.stringify(response) === JSON.stringify(expected_response))
    assert(response.number === 42)
    assert(response.string === 'heyho')
    RD.disconnect()
    LOG = []
}

test()
