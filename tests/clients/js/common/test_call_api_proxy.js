const {RD, CallApi} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test_call_via_api_object() {
    await RD.connect('ws://localhost:37007', null, logger)
    let response = await CallApi().TestApi.hello()
    assert.equal('hello', response)

    console.log('Network traffic', LOG)
    assert(LOG.includes('>>> {"cmd":"TestApi.hello()","type":1,"id":1}'))
    assert(LOG.includes('<<< {"id": 1, "data": "hello", "type": 2}'))
    RD.disconnect()
    LOG = []
}

test_call_via_api_object()
