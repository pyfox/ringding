const {RD} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    await RD.connect('ws://localhost:37007', null, logger)
    let response = await RD.call('TestApi.get_user(*id).give_cookie(*c)', {
        'id': {'user_id': 1},
        'c': {'cookie': 'chocolate'}
    })
    assert.equal('Thank you for a chocolate cookie :)', response)
    console.log('Network traffic', LOG)
    RD.disconnect()
}
test()
