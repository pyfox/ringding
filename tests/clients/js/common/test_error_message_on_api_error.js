const {RD} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    const invalid_user_id = 84324384234
    await RD.connect('ws://localhost:37007', null, logger)
    try {
        await RD.call('TestApi.get_user(*)', {'user_id': invalid_user_id})
        caught_exception = false
    } catch (e) {
        caught_exception = true
        assert(String(e).includes(`Error while executing get_user({\'user_id\': ${invalid_user_id}}) of TestApi.get_user(*): Did not find a user with ID ${invalid_user_id}`))
    } finally {
        console.log('Network traffic', LOG)
    }
    assert(caught_exception)
    RD.disconnect()
}

test()
