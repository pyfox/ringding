const {RD, CallApi} = require(process.env.RINGDINGJS_TEST_CLIENT_PATH)
const assert = require('assert')

let LOG = []

function logger(message) {
    LOG.push(message)
}

async function test() {
    await RD.connect('ws://localhost:37007', null, logger)
    let response = await CallApi().TestApi.DataTypeApi.get_object()
    console.log('Network traffic', LOG)
    assert(response === 'I am some object')
    RD.disconnect()
    LOG = []
}

test()
