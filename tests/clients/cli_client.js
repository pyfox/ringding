/**
 * This is a test client for ringding. It is a node-based JavaScript client that
 * calls a command on a server. The command and its parameters are extracted from the
 * command line arguments. The parameters are formatted in JSON with escaped quotes.
 *
 * Usage: node cli_client "my.command.name(*)" "\"{\"a\": 42}\""
 */
const { RD } = require('../../clients/js/ringdingjs')

if (process.argv.length === 2) {
    throw 'You have to provide a message to send!'
}

const command = process.argv[2]
const parameters = process.argv[3]

async function communicate() {
    console.log("call", command, "with", parameters)
    await RD.connect('ws://localhost:36097', null, console.log)
    await RD.send_message(command, JSON.parse(parameters || '{}'))
    RD.disconnect()
}

communicate()
